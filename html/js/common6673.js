﻿'use strict';
var didScroll;
var deviceCheker;
var IOSCheker;
var navCheker = false;
var headerPosition = 90;
function _isMobile() {// 모바일 여부 체크
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent)) {
        deviceCheker = true;
        $('#wrapper').addClass('isMobile');
    }
}

function _isIOS() {// 아이폰 디바이스 체크
    if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
        IOSCheker = true;
        $('#wrapper').addClass('isIOS');
    }
}


function scrollHeader() { // 모바일 Scroll Up/Down 체크

    var lastScrollTop = 0;
    var delta = 71;
    var navbarHeight = 50;

    setInterval(function () {
        if (didScroll) {
            hasScrolled();
            didScroll = false;
        }
    }, 250);

    function hasScrolled() {
        var st = $(window).scrollTop();
        if (Math.abs(lastScrollTop - st) <= delta)
            return;
        if (st > lastScrollTop && st > navbarHeight) {
            // Scroll Down
            if (navCheker == false) {
                $('#header').removeClass('nav-down').addClass('nav-up');
            } else {
                return false
            }
        } else {
            // Scroll Up
            if (st + $(window).height() < $(document).height()) {
                $('#header').removeClass('nav-up').addClass('nav-down');
            }
        }
        lastScrollTop = st;
    }

}


function MnavFn() {//모바일 네비 동작
    function openNav() { //네비 열림
        $('.mobileHeader nav').css('left', 0);
        $('.mobileHeader').append('<div class="dimmed"></div>');
        $('.dimmed').fadeIn(400);
        $('body').css('overflow', 'hidden');
        navCheker = true;
    }
    function closeNav() { //네비 닫음
        $('.mobileHeader nav').css('left', '-100%');
        $('.dimmed').remove();
        $('body').removeAttr('style');
        navCheker = false;
    }


    $(document).on('click', '.openBtn', function () {
        $(this).addClass('closeBtn');
        $(this).removeClass('openBtn');
        openNav();
    });

    $(document).on('click', '.closeBtn,.dimmed', function () {
        closeNav();
        $('.closeBtn').addClass('openBtn');
        $('.closeBtn').removeClass('closeBtn');
    });


}

function focusInpt() { //input 포커스 동작
    $(document).on('focusin', '.formInput input', function () {
        $(this).siblings('label').hide();
        $(this).parent('.formInput').addClass('focus');
    });

    $(document).on('focusout', '.formInput input', function () {
        if ($(this).val() === '') {
            $(this).siblings('label').show();
            $(this).parent('.formInput').removeClass('focus');
        }
    });
}




function popup(msg, callback) {
    popup.callback = null;
    if (msg[0] == '#') {
        $(msg).fadeIn(200);
    }
    else {
        var str =
        '<div id="alertPop" class="popup alertPop">' +
            '<div class="content instant after">' +
                '<a href="javascript:void(0);" class="btn btnClose">' +
                    '<span>Close</span>' +
                '</a>' +
                '<div class="container">' +
                    '<p>' + msg + '</p>' +
                    '<button class="btnDone" type="button"></button>' +
                '</div>' +
            '</div>' +
        '</div>';

        $('#alertPop').remove();
        $('body').append(str);
        $('#alertPop').fadeIn(200);
    }

    if (callback && {}.toString.call(callback) === '[object Function]') {
        popup.callback = callback;
    }
}


// send to SNS
function toSNS(sns, strURL) {
    var snsArray = new Array();
    snsArray['twitter'] = "http://twitter.com/share?url=" + encodeURIComponent(strURL);
    snsArray['facebook'] = "http://www.facebook.com/sharer.php?u=" + encodeURIComponent(strURL);
    snsArray['google'] = "https://plus.google.com/share?url=" + encodeURIComponent(strURL);
    window.open(snsArray[sns], '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
}

//Top으로 이동
function goTotop() {
    var offset = 400;
    var duration = 500;
    $('.btn_gotoTop').click(function (event) {
        event.preventDefault();
        $('html, body').animate({ scrollTop: 0 }, duration);
        return false;
    });
}

/* 팝업 */
var element = '';
function formatZero(data) {
    return data.toString().length > 1 ? data : '0' + data;
}
function imagePopUp(url, imageLength) {
    $('#imagePop').remove();
    element = '.imageSection';
    var imageList = [];
    var modalTemp = '';
    modalTemp += '<div id="imagePop" class="mediaPopup blkPop imagePop"><div class="contents">';
    modalTemp += '<div id="racoon" class="imageSection owl-carousel owl-theme">';

    for (var i = 0; i < imageLength; i++) {
        modalTemp += '<div>';
        modalTemp += '<img src="' + url + formatZero(i) + '.jpg' + '" />';
        modalTemp += '</div>';
    }

    modalTemp += '</div><div class="btnPop"><ul class="btnSocial">';
    modalTemp += '</ul><p class="button btnClose_w"><a href="#none" class="btnClose"><span>Close</span></a></p></div></div></div>';

    $("body").append(modalTemp);
    $("body").append(function () {
        $(".blkPop#imagePop").fadeIn(200);

        $('#racoon').owlCarousel({
            startPosition: 0,
            loop: true,
            nav: true,
            items: 1,
            mouseDrag: true,
            touchDrag: true,
            dots: true,
            onInitialized: function () {
            }
        });
    });
}
function moviePopUp(url) {
    $('#moviePop').remove();
    element = '.movieSection';    

    var modalTemp = '';
    modalTemp += '<div id="moviePop" class="mediaPopup blkPop moviePop" style="display:none"><div class="contents">';
    modalTemp += '<div class="movieSection"><iframe id="class_video_pop" src="' + url + '?autoplay=1&controls=1&showinfo=0&rel=0&modestbranding=1&" frameborder="0" allowfullscreen></iframe></div>';
    modalTemp += '<div class="btnPop"><ul class="btnSocial">';
    modalTemp += '<li class="btnSocialGoogle"><a onclick="toSNS(\'google\',\'' + url + '\')" class="ir" title="googleplus">구글플러스 공유하기</a></li>';
    modalTemp += '<li class="btnSocialFbook"><a onclick="toSNS(\'facebook\',\'' + url + '\')" class="ir" title="facebook">페이스북 공유하기</a></li>';
    modalTemp += '<li class="btnSocialTwit"><a onclick="toSNS(\'twitter\',\'' + url + '\')" class="ir" title="twitter">트위터 공유하기</a></li>';
    modalTemp += '</ul><p class="button btnClose_w"><a href="#none" class="btnClose"><span>Close</span></a></p></div></div></div>';

    $("body").append(modalTemp);
    $("body").append(function () {
        $(".blkPop#moviePop").fadeIn();
    });
}


function detailPopUp(elem) {
    $('#detailPop').remove();

    var className = $(elem).siblings('strong').find('em').text();
    var classweapon = $(elem).parent('.classTitle').siblings('.classDetail').find('.c_weapon').text();
    var classtype = $(elem).parent('.classTitle').siblings('.classDetail').find('.type').text();
    var classdesc = $(elem).parent('.classTitle').siblings('.classDetail').find('.desc').find('p').text();

    var modalTemp = '';
    modalTemp += '<div class="detailInfo blkPop" id="detailPop"><div class="contents"><p class="btn btnClose"><a href="javascript:void(0);">close</a></p>';
    modalTemp += '<div class="name light">';
    modalTemp += className;
    modalTemp += '</div>';
    modalTemp += '<div class="description">';
    modalTemp += classdesc;
    modalTemp += '</div>';
    modalTemp += '<div class="battleInfo1">代表武器</div>';
    modalTemp += '<div class="battleInfo1_answer">';
    modalTemp += classweapon;
    modalTemp += '</div>';
    modalTemp += '<div class="battleInfo2">戰鬥型態</div>';
    modalTemp += '<div class="battleInfo2_answer">';
    modalTemp += classtype;
    modalTemp += '</div>';
    modalTemp += '</div></div>';

    $("body").append(modalTemp);
    $("body").append(function () {
        $(".blkPop#detailPop").fadeIn();
    });

}


function galleryOpen(obj, imageLength) {
    var url = _cdn + 'Common/img/origin/class/' + obj + '/';
    var count = imageLength;
    imagePopUp(url, count);
}

function contentsOpen(obj, imageLength) {
    var url = _cdn + 'Common/img/origin/contents/' + obj + '/';
    var count = imageLength;
    imagePopUp(url, count);
}

function movieOpen(url) {
    var youtube = url;
    moviePopUp(youtube);
}

function closePopup(target) {
    $(target).fadeOut(300, function () {
        if ($(this).attr('id') == 'movie') {
            $('#moviePop .movieSection').empty();
        }
    });
    element = '';
}


$(window).on('load', function () {
    _isMobile();
    _isIOS();
    MnavFn();//모바일 네비 동작
    focusInpt();
    goTotop();
    if (deviceCheker == true) {
        headerPosition = 50;

    } else {
        headerPosition = 90;
    }
    scrollHeader();
    $('.formInput input').each(function () {
        if ($(this).val()) {
            $(this).siblings('label').hide();
            $(this).parent('.formInput').addClass('focus');
        }
    });
    $(document).on('click', '.popup', function () {
        closePopup(this);
    });

    $(document).on('click', '.popup .btnClose, .btnDone', function (e) {
        closePopup('.popup');
    });

    $(document).on('click', '.blkPop .btnClose', function (e) {
        $('.blkPop').fadeOut(function () {
            $('.blkPop').remove();
        });
    });

    $(document).on('click', '.mediaPopup', function (e) {
        $('.mediaPopup').fadeOut(function () {
            $('.mediaPopup').remove();
        });
    });


    $(document).on('click', '.blkPop .contents, .mediaPopup .detailInfo', function (e) {
        e.preventDefault();
        e.stopPropagation();
    });
    $(document).on('click', '.popup .content', function (e) {
        e.preventDefault();
        e.stopPropagation();
    });


    //링크이동시 메뉴 active 처리
    //var url = window.location.pathname,
    //       urlRegExp = new RegExp(url.replace(/\/$/, ''));

    //$('.depth01 li a').each(function () {
    //    console.log(urlRegExp.test(this.href));
    //    if (urlRegExp.test(this.href)) {
    //        $(this).parent('li').siblings('li').removeClass('active');
    //        $(this).parent('li').addClass('active');
    //    }
    //});


});


$(window).on('scroll', function () { //스크롤 체크
    didScroll = true;
});