﻿'use strict';

$(window).on('load', function () {

    var wW = $(window).outerWidth();
    $('.bgslide .slider').css('width', wW)


    var visualSwiper = new Swiper('.visualWrapper .swiper-container', {//메인 최상단 슬라이드
        slidesPerView: 1,
        loop: true,
        paginationClickable: true,
        pagination: {
            el: '.visualWrapper .swiper-pagination',
            clickable: true
        },
        navigation: {
            nextEl: '.navNext',
            prevEl: '.navPrev'
        }
    });

    createSwiper();
    function createSwiper() {//메인 금수랑 슬라이드 생성
        var bgSwiper = undefined;
        var bigSwiper = undefined;

        function initSwiper() {
            var wW = $(window).width();
            if (wW > 1170) {
                if (bgSwiper == undefined) {
                    bgSwiper = new Swiper('.bgslide .slider', {
                        slidesPerView: 'auto',
                        centeredSlides: true,
                        loop: true,
                        loopedSlides: 5,
                        loopAddtionalSlides: 5,
                        paginationClickable: true,
                        pagination: '.bgslide .swiper-pagination',
                        speend: 1200

                    });
                    bigSwiper = new Swiper('.phoneSlide .slider', {
                        slidesPerView: 1,
                        loop: true,
                        loopedSlides: 5,
                        loopAddtionalSlides: 5,
                        paginationClickable: true,
                        pagination: '.phoneSlide .swiper-pagination',
                        speend: 1200
                    });
                    bgSwiper.controller.control = bigSwiper;

                    $('.btnList a').on('mouseenter', function () {
                        if(bgSwiper !== undefined){
                            var idx = $('.btnList a').index(this);
                            var item = $('.slideCellphone .phoneSlide .swiper-wrapper');
                            $('.btnList a').removeClass('active');
                            $('.btnList a').eq(idx).addClass('active');
                            if (item.is(':animated')) {
                                return false
                            } else {
                                bgSwiper.slideToLoop(idx)
                            }
                        }
                    });

                }
            } else if (wW < 1170) {
                if(bgSwiper !== undefined){
                bgSwiper.destroy(true);
                    bigSwiper.destroy(true);
                    bgSwiper = undefined;
                    bigSwiper = undefined;
                    $('.swiper-container').removeAttr('style');
                    $('.swiper-wrapper').removeAttr('style');
                    $('.swiper-slide').removeAttr('style');
                    }
                return
            }
        }
        initSwiper();
         $(window).on('resize', function () { //리사이즈 체크
            initSwiper();
        });
    }
});

$(function () {
    $('i[id^="btnVideoPlay_"]').on('click', function (e) {
        var _url = $(this).data('videolink');

        if (_url != '') {
            movieOpen($(this).data('videolink'));
        }
        return false;
    })

    $('a[id^="btnBannerArea_"]').on('click', function () {
        var _data = $(this).data();

        if (_data.isevent == 'True') {
            ga('send', 'event', _data.eventcategory, 'click', _data.eventlabe, 1);
        }

        location.href = _data.url;
    })
})