<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">

		<div class="entry-info">
			<?php thesimplest_entry_meta(); ?>
		</div>
	</header>

    <div class="entry-content">
	    <?php
	    $content    =   apply_filters( 'the_content', get_the_content() );
	    $video      =   false;
	    if( !strpos( $content, 'wp-playlist-script' ) ) {
		    $audio  =   get_media_embedded_in_content( $content, array(
			    'audio', 'iframe'
		    ) );
	    }
	    if( $audio ) {
		    printf( '<div class="entry-audio">%1$s</div>', $audio[0] );
	    }
	    ?>
	    <?php thesimplest_excerpt(); ?>
    </div><!-- .entry-content -->

    <?php thesimplest_entry_footer(); ?>

</article>
