</div><!-- .site-content -->

<?php
/**
 * In case you want to disable the footer.
 */
$thesimplest_footer = apply_filters('thesimplest_display_footer', true);

if ($thesimplest_footer) :
    ?>
<footer id="colophon" class="site-footer" role="contentinfo">

</footer>
<?php
endif;
?>

</div><!-- site-inner -->
</div><!-- site -->

<?php wp_footer(); ?>
</body>

</html>