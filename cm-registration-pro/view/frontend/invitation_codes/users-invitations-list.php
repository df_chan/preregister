<?php

use com\cminds\registration\model\InvitationCode;
use com\cminds\registration\model\Labels;
use com\cminds\registration\model\User;

?>
<div class="cmreg-list-users-invitations-shortcode">

    <?php if (!empty($codes)) : ?>


    <table>
        <thead>
            <tr>
                <th data-col="code" style="text-align:center;">
                    <?php echo Labels::getLocalized('invitation_code_str'); ?>
                </th>
                <!-- <th data-col="email-addr"><?php echo Labels::getLocalized('invitation_email_addr'); ?></th> -->
                <!-- <th data-col="user-role"><?php echo Labels::getLocalized('invitation_user_role'); ?></th> -->
                <!-- <th data-col="expiration-date"><?php echo Labels::getLocalized('invitation_expiration_date'); ?></th> -->
                <th data-col="users-limit" style="text-align:center">
                    <?php echo Labels::getLocalized('invitation_users_limit'); ?>
                </th>
                <!-- <th data-col="email-verification"><?php echo Labels::getLocalized('invitation_email_verification'); ?></th> -->
                <th data-col="created-date" style="text-align:center">
                    <?php echo Labels::getLocalized('invitation_created_date'); ?>
                </th>
                <!-- <th data-col="used"><?php echo Labels::getLocalized('invitation_used'); ?></th> -->
                <th data-col="copy-title" style="text-align:center">
                    초대 URL 복사
                </th>
            </tr>
        </thead>
        <tbody><?php foreach ($codes as $code) : ?>
            <?php /* @var $code InvitationCode */ ?>
            <div id="invisibleurlcopy" style="font-size: xx-small;color:#141325;">
                https://www.digifinexkorea.com/prejoin.html?invitecode=<?php echo esc_html($code->getCodeString()); ?>
            </div>
            <tr>
                <td data-col="code" style="text-align:center;" width="300px">
                    <div id="codecopy">
                        <?php echo esc_html($code->getCodeString()); ?>
                    </div>
                </td>
                <!-- <td data-col="email-addr"><?php echo esc_html($code->getRequiredEmail()); ?></td> -->
                <!-- <td data-col="user-role"><?php $role = $code->getUserRole();
                                                        echo esc_html(User::getRoleNameByKey($role ? $role : $defaultRole)); ?></td> -->
                <!-- <td data-col="expiration-date"><?php $date = $code->getExpirationDateFormatted();
                                                            echo esc_html($date ? $date : Labels::getLocalized('invitation_expires_never')); ?></td> -->
                <td data-col="users-limit" style="text-align:center">
                    <?php $limit = $code->getUsersLimit();
                            echo esc_html($code->getUsersCount() . '명 ' . ($limit ? $limit : Labels::getLocalized('invitation_unlimited_users'))); ?>
                </td>
                <!-- <td data-col="email-verification">
													                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <?php echo esc_html($code->getEmailVerificationStatusOrGlobal() ? Labels::getLocalized('invitation_verify_email_required') : Labels::getLocalized('invitation_verify_email_not_required')); ?>
										                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                </td> -->
                <td data-col="created-date" style="text-align:center">
                    <?php echo esc_html($code->getCreatedDate()); ?>
                </td>
                <!-- <td data-col="used">                                                                                                                                                   </td> -->
                <td data-col="copy" style="text-align:center">
                    <button class="btn" data-clipboard-action="copy" data-clipboard-target="#codecopy">Copy</button>
                    <button class="btn" data-clipboard-action="copy"
                        data-clipboard-target="#invisibleurlcopy">URL복사</button>
                    <script type="text/javascript" src="../js/clipboard.min.js"></script>
                    <script>
                    var clipboard = new ClipboardJS('.btn');

                    clipboard.on('success', function(e) {
                        console.log(e);
                    });

                    clipboard.on('error', function(e) {
                        console.log(e);
                    });
                    </script>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <?php else : ?>
    <p><?php echo Labels::getLocalized('invitation_list_empty'); ?></p>
    <?php endif; ?>
</div>