<?php

use com\cminds\registration\model\InvitationCode;
use com\cminds\registration\model\Labels;

?>
<div class="cmreg-create-invitation-code-result">
    <p>초대코드:</p>
    <div id="onlycodecopy" class="cmreg-invitation-code-string"><?php echo $codeString; ?></div>
    <br>
    <button class="btn" data-clipboard-action="copy" data-clipboard-target="#onlycodecopy">초대코드 복사</button>
    <script type="text/javascript" src="../js/clipboard.min.js"></script>
    <script>
    var clipboard = new ClipboardJS('.btn');

    clipboard.on('success', function(e) {
        console.log(e);
    });

    clipboard.on('error', function(e) {
        console.log(e);
    });
    </script>

    <?php if ($sentByEmail) : ?>
    <p>The invitation link has been send to the specified email address.</p>
    <?php endif; ?>
</div>