<?php
use com\cminds\registration\controller\InvitationCodesController;
use com\cminds\registration\model\Labels;
?>
<?php
$icode = $_GET[invitecode];
if ($icode == "") {
    $icode = "\"";
} else {
    $icode = $icode . "\" readonly";
}
?>
<div class="cmreg-invitation-code-field"
    data-input-visible="<?php echo intval($invitationCodeRequired or !empty($invitationCode)); ?>">
    <a href="123"><?php echo Labels::getLocalized('register_invitation_code_link'); ?></a>
    <input type="text" class="text" name="<?php echo InvitationCodesController::FIELD_INVITATION_CODE; ?>"
        <?php echo ($invitationCodeRequired ? 'required' : ''); ?>
        placeholder="<?php echo esc_attr(Labels::getLocalized('field_invitation_code')); ?>" value="<?php echo "$icode" ?> />
    <?php if ($invitationCodeTooltip != '') { ?>
                            <span class=" cmreg-field-description"><?php echo $invitationCodeTooltip; ?></span>
    <?php } ?>
</div>