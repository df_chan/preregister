<?php

use com\cminds\registration\model\Labels;

?>
<div class="cmreg-overlay">
    <div class="cmreg-overlay-inner" style="background-color:#E0E0E0">
        <span class="cmreg-overlay-close" style="color:#777777"
            title="<?php echo esc_attr(Labels::getLocalized('close')); ?>">&times;</span>
        <?php echo $content; ?>
    </div>
</div>