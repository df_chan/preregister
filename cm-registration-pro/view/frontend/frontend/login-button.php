<?php
$redirect_url = '';
if(esc_attr($atts['redirect-to']) != '') {
	if(strpos(esc_attr($atts['redirect-to']), "http") !== false) {
		$redirect_url = esc_attr($atts['redirect-to']);
	} else {
		$redirect_url = site_url().esc_attr($atts['redirect-to']);
	}
}
?>
<a href="<?php echo esc_attr($href); ?>" class="cmreg-login-button cmreg-login-click<?php if($extraClass != '') { echo ' '.$extraClass; } ?>" redirect_to="<?php echo $redirect_url; ?>" after_login="<?php echo $atts['after-login']; ?>" after_text="<?php echo $atts['after-text']; ?>"><?php echo $loginButtonText; ?></a>