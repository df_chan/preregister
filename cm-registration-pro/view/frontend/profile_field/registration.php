<?php
use com\cminds\registration\model\Labels;
use com\cminds\registration\model\ProfileField;
use com\cminds\registration\helper\FormBuilderRender;
/* @var $field ProfileField */

$label = $field->getLabel();
if ($field->isRequired()) {
	$label .= ' ' . Labels::getLocalized('field_required');
}
$containter_class = '';
$containter_class = $field->getUserMetaKey();
if ($containter_class != '') {
	$containter_class = ' ' . $containter_class . '_rowcontainer';
}
$field_class = '';
$field_class = $field->getCSSClass();
if ($field_class != '') {
	$field_class = str_replace('form-control', '', $field_class);
	$field_class = ' ' . trim($field_class);
}
?>
<div class="cmreg-registration-field<?php echo $containter_class; ?><?php echo $field_class; ?>">
    <?php echo FormBuilderRender::render('cmreg_extra_field', $field); ?>
</div>