<?php
use com\cminds\registration\controller\RegistrationController;
use com\cminds\registration\model\Settings;
use com\cminds\registration\model\Labels;

$redirectUrl = '';
if (esc_attr($atts['redirect-to']) != '') {
    if (strpos(esc_attr($atts['redirect-to']), "http") !== false) {
        $redirectUrl = esc_attr($atts['redirect-to']);
    } else {
        $redirectUrl = site_url() . esc_attr($atts['redirect-to']);
    }
}
$cmreg_redirect_url = filter_input(INPUT_GET, 'cmreg_redirect_url');
if ($cmreg_redirect_url != '') {
    $redirectUrl = $cmreg_redirect_url;
}
?>
<script type="text/javascript">
function inNumber() {
    if (event.keyCode < 48 || event.keyCode > 57) {
        event.returnValue = false;
    }
}
</script>
<div class="cmreg-registration cmreg-wrapper">
    <form method="post" data-ajax-url="<?php echo esc_attr(admin_url('admin-ajax.php')); ?>"
        class="cmreg-form cmreg-registration-form">

        <!--        <h2 style="color:#777777;"><?php echo Labels::getLocalized('register_form_header'); ?></h2> -->
        <div class="cmreg-form-text"><?php echo Labels::getLocalized('register_form_text'); ?></div>

        <?php do_action('cmreg_register_form', 'cmreg_overlay', $atts); ?>

        <div class="cmreg-buttons-field">
            <input type="hidden" style="width:15em" name="action" value="cmreg_registration" />
            <input type="hidden" style="width:15em" name="cmreg_redirect_url"
                value="<?php echo esc_attr($redirectUrl); ?>" />
            <input type="hidden" style="width:15em" name="<?php echo esc_attr(RegistrationController::FIELD_ROLE); ?>"
                value="<?php echo esc_attr($atts['role']); ?>" />
            <input type="hidden" style="width:15em"
                name="<?php echo esc_attr(RegistrationController::FIELD_ROLE_NONCE); ?>"
                value="<?php echo esc_attr($roleNonce); ?>" />
            <input type="hidden" style="width:15em" name="nonce" value="<?php echo $nonce; ?>" />
            <?php echo "<p>* 사전예약 참여시에 마케팅 정보제공 동의하는<br>것으로 간주합니다.</p>" ?>
            <button type="submit" onclick="register_click();">
                <span
                    class="dashicons dashicons-edit"></span><?php echo Labels::getLocalized('register_form_submit_btn'); ?>
            </button>
            <!--        <script>
            function register_click() {
                var email_value = document.getElementById("emailID").value;
                var name_value = document.getElementById("nameID").value;
                var pw_value = document.getElementById("cmregpwID").value;
                var pwa_value = document.getElementById("cmregpwagainID").value;
                var id_value = document.getElementById("idID").value;
                var check_value = "refresh";

                if (email_value == "" || email_value == null || email_value == undefined) {
                    check_value = "";
                }
                if (name_value == "" || name_value == null || name_value == undefined) {
                    check_value = "";
                }
                if (pw_value == "" || pw_value == null || pw_value == undefined) {
                    check_value = "";
                }
                if (pwa_value == "" || pwa_value == null || pwa_value == undefined) {
                    check_value = "";
                }
                if (id_value == "" || id_value == null || id_value == undefined) {
                    check_value = "";
                }
                if (check_value == "refresh") {
                    // alert('전부다 입력되었으므로 초기화합니다.');
                    setTimeout(function() {
                        window.location.href = '../redirect_prejoin.html';
                    }, 4000);
                }

            }
            </script>
        -->
        </div>

        <?php do_action('cmreg_register_form_bottom', $atts); ?>

        <?php if (isset($atts['login-url'])) : ?>
        <div class="cmreg-login-link"><a href="<?php echo esc_attr($atts['login-url']); ?>">
                <?php echo (isset($atts['login-link']) ? $atts['login-link'] : Labels::getLocalized('registration_login_btn')); ?></a>
        </div>
        <?php endif; ?>

    </form>
</div>