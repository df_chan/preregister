<?php
use com\cminds\registration\controller\LoginController;
use com\cminds\registration\controller\EmailVerificationController;
use com\cminds\registration\model\Labels;
use com\cminds\registration\model\Settings;
use com\cminds\registration\model\ProfileField;

$loginField = Settings::getOption(Settings::OPTION_LOGIN_FIELD);
$loginFieldLabel = Labels::getLocalized('login_field_' . $loginField);
$loginFieldType = ($loginField == Settings::LOGIN_FIELD_EMAIL ? 'email' : 'text');
$redirectUrl = '';
if (esc_attr($atts['redirect-to']) != '') {
    if (strpos(esc_attr($atts['redirect-to']), "http") !== false) {
        $redirectUrl = esc_attr($atts['redirect-to']);
    } else {
        $redirectUrl = site_url() . esc_attr($atts['redirect-to']);
    }
}
$cmreg_redirect_url = filter_input(INPUT_GET, 'cmreg_redirect_url');
if ($cmreg_redirect_url != '') {
    $redirectUrl = $cmreg_redirect_url;
}
?>
<script type="text/javascript">
function windowopenPopupPrivacy() {
    window.open('../privacy-agreement/', 'windowpopup',
        'width=600, height=800, scrollbars=yes, menubar=no, status=no, toolbar=no');
}

function windowopenPopupService() {
    window.open('../service-agreement/', 'windowpopup',
        'width=600, height=800, scrollbars=yes, menubar=no, status=no, toolbar=no');
}

function windowopenPopupMarketing() {
    window.open('../marketing-agreement/', 'windowpopup',
        'width=600, height=800, scrollbars=yes, menubar=no, status=no, toolbar=no');
}
</script>
<div class="cmreg-login cmreg-wrapper">
    <form method="post" data-ajax-url="<?php echo esc_attr(admin_url('admin-ajax.php')); ?>"
        class="cmreg-form cmreg-login-form">
        <h2 style="color:#777777"><?php echo Labels::getLocalized('login_form_header'); ?></h2>
        <div class="cmreg-form-text"><?php echo Labels::getLocalized('login_form_text'); ?></div>
        <div class="cmreg-login-field"><input type="<?php echo $loginFieldType; ?>" class="text" name="login" required
                placeholder="<?php echo esc_attr($loginFieldLabel); ?>" />
        </div>
        <div class="cmreg-password-field"><input type="password" class="text"
                name="<?php echo esc_attr(ProfileField::REGISTRATION_FORM_ROLE_PASSWORD); ?>" required
                placeholder="<?php echo esc_attr(Labels::getLocalized('field_password')); ?>" />
        </div>
        <div class="cmreg-2fa-field" style="display:none;">
            <input type="text" class="text" name="two_factor_code" placeholder="One Time Password (i.e. 2FA)" />
        </div>
        <?php if (Settings::getOption(Settings::OPTION_LOGIN_REMEMBER_ENABLE)) : ?>
        <div class="cmreg-remember-field"><label><input type="checkbox" name="remember" value="1" />
                <?php
                    echo Labels::getLocalized('login_form_remember'); ?></label></div>
        <?php endif; ?>
        <?php do_action('login_form', 'cmreg_overlay'); ?>
        <div class="cmreg-buttons-field">
            <input type="hidden" name="action" value="cmreg_login" />
            <input type="hidden" name="cmreg_redirect_url" value="<?php echo esc_attr($redirectUrl); ?>" />
            <input type="hidden" name="nonce" value="<?php echo $nonce; ?>" />
            <button type="submit"><span
                    class="dashicons dashicons-admin-users"></span><?php echo Labels::getLocalized('login_form_submit_btn'); ?></button>
        </div>
        <div><a href="javascript:windowopenPopupPrivacy();">개인정보 처리방침 자세히 보기</a></div>
        <div><a href="javascript:windowopenPopupService();">서비스 이용약관 자세히 보기</a></div>
        <div><a href="javascript:windowopenPopupMarketing();">마케팅 정보이용 자세히 보기</a></div>
        <?php if (!Settings::getOption(Settings::OPTION_PREVENT_CALLING_LOGIN_FOOTER_FRONTEND)) : ?>
        <?php do_action('login_footer'); ?>
        <?php endif; ?>
        <?php do_action('cmreg_login_form_bottom', $atts); ?>
    </form>

    <?php if (isset($atts['registration-url'])) : ?>
    <div class="cmreg-registration-link"><a
            href="<?php echo esc_attr($atts['registration-url']); ?>"><?php echo (isset($atts['registration-link']) ? $atts['registration-link'] : Labels::getLocalized('login_registration_btn')); ?></a>
    </div>
    <?php endif; ?>
    <?php do_action('cmreg_login_wrapper_bottom', $atts); ?>

</div>