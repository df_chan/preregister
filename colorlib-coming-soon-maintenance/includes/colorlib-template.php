<!DOCTYPE html>
<html lang="ko">

<head>
    <meta charset="utf-8">
    <title><?php bloginfo('name');
            $site_description = get_bloginfo('description'); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="디지파이넥스코리아 사전예약 페이지" />
    <meta name="naver-site-verification" content="68b99b2e8b78913354086ca10d52121f" /> <!-- 네이버 웹마스터 도구-->
    <meta property="og:type" content="website" />
    <meta property="og:title" content="디지파이넥스코리아 사전예약" />
    <meta property="og:description" content="디지파이넥스코리아, 디파코, 사전예약, 암호화폐, 가상화폐, 비트코인, 이더리움, 비트코인캐시, 리플, 라이트코인" />
    <meta property="og:image" content="http://digifinexkorea.com/img/link_preview.jpg" />
    <meta property="og:url" content="http://digifinexkorea.com" />
    <?php
    $ccsm_options = get_option('ccsm_settings');

    if ($ccsm_options['colorlib_coming_soon_template_selection']) {
        $template = $ccsm_options['colorlib_coming_soon_template_selection'];
    }

    $counterActivation = $ccsm_options['colorlib_coming_soon_timer_activation'];
    do_action('ccsm_header', $template);

    ?>
    <style>
    <?php if ($counterActivation !='1') {
        ?>.cd100 {
            display: none !important;
        }

        <?php
    }

    if (ccsm_template_has_background_color()) {
        ?>body {
            background-color: <?php echo $ccsm_options['colorlib_coming_soon_background_color'];
            ?> !important;
        }

        <?php
    }

    ?><?php echo $ccsm_options['colorlib_coming_soon_page_custom_css'];

    ?>.colorlib-copyright {
        text-align: center;
        left: 0;
        right: 0;
        margin: 0 auto;
    }

    .colorlib-copyright span {
        opacity: 0.8;
    }

    .colorlib-copyright a {
        opacity: 1;
    }
    </style>
</head>

<body>
    <script language="JavaScript">
    function startTime() {
        var time = new Date();
        hours = time.getHours();
        mins = time.getMinutes();
        secs = time.getSeconds();
        closeTime = hours * 3600 + mins * 60 + secs;
        closeTime += 5; //시간설정
        Timer();
    }

    function Timer() {
        var time = new Date();
        hours = time.getHours();
        mins = time.getMinutes();
        secs = time.getSeconds();
        curTime = hours * 3600 + mins * 60 + secs
        if (curTime >= closeTime) {
            document.all['divpop'].style.visibility = "hidden";
        } else {
            window.setTimeout("Timer()", 1000)
        }
    }


    function setCookie(name, value, expiredays) {
        var todayDate = new Date();
        todayDate.setDate(todayDate.getDate() + expiredays);
        document.cookie = name + "=" + escape(value) + "; path=/; expires=" + todayDate.toGMTString() + ";"
    }

    function closeWin() {
        if (document.notice_form.chkbox.checked) {
            setCookie("maindiv", "done", 1);
        }
        document.all['divpop'].style.visibility = "hidden";
    }
    </script>
    <div id="divpop" style="position:absolute;left:350px;top:250px;z-index:200;visibility:hidden;">
        <table width=500px height=200px cellpadding=2 cellspacing=0>
            <tr>
                <td style="color:#eee; font-family: 'Noto Sans KR', 'NanumSquareRound'; border:1px #666666 solid;font-size:16px"
                    height=160px bgcolor=#202639>
                    <P align=center><strong>안내</strong></P><br>
                    전 세계 디지털거래의 중심 디지파이넥스코리아입니다.<BR>
                    저희 사전가입 이벤트에 많은 관심을 가져 주셔서 진심으로 감사드립니다.<BR>
                    현재 암호화폐 시장을 고려하여 안전한 시스템 운영을 위해 시스템을 구축하였으나<BR>
                    너무나도 많은 분들이 관심을 가져 주신 나머지 서버 폭주시마다 증설하는 방향이 아닌<BR>
                    또한번의 불편함이 없도록 서버를 추가적으로 증설하기로 하였습니다. <bR>
                    추가적인 증설이 완료되는 시점은 06월 11일 14시입니다.<BR>
                    불편을 드려 죄송하며 넘쳐나는 관심에 다시한번 감사의 말씀을 올립니다.<BR><br>
                    - 디지파이넥스코리아 임직원일동 -
                </td>
            </tr>
            <tr>
                <form name="notice_form">
                    <td align=right bgcolor=#666875 style="color:#eee; font-family: 'Noto Sans KR', 'NanumSquareRound';"">
                <input type=" checkbox" name="chkbox" value="checkbox">오늘 하루 이 창을 열지 않음
                        <a href="javascript:closeWin();"><strong>[닫기]</strong></a>
                    </td>
            </tr>
            </form>
        </table>
    </div>
    <?php include(CCSM_PATH . 'templates/' . $template . '/' . $template . '.php'); ?>

</body>

</html>